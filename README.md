[![pipeline status](https://framagit.org/mieg/prgconfig/badges/master/pipeline.svg)](https://framagit.org/mieg/prgconfig/pipelines)
[![coverage report](https://framagit.org/mieg/prgconfig/badges/master/coverage.svg)](https://framagit.org/mieg/prgconfig/pipelines)

prgconfig
=========

prgconfig is a little library that ease the manage of configuration
file written in toml. It comes with nice default. The minimum you have
to specify is your program name then it does the rest. It aims to fit to
standard in use for location of configuration file. It is also totally
configurable to fit your needs.
