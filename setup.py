from setuptools import find_packages, setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="prgconfig",
    version="1.0.0-beta.1",
    description="Configuration manager using toml language.",
    author="Rémy Taymans",
    author_email="remy.mieg@gmail.com",
    url="https://framagit.org/mieg/prgconfig",
    license="GPL-3.0+",
    long_description=long_description,
    long_description_content_type="text/markdown",
    package_dir={"": "src"},  # Set src as root for finding packages
    py_modules=["prgconfig"],
    install_requires=["toml"],
    provides=["prgconfig"],
    python_requires=">=3.5",
    classifiers=[
        "Intended Audience :: Developers",
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved "
        ":: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: Unix",
        "Operating System :: POSIX",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Topic :: Software Development :: Libraries",
    ],
    project_urls={
        "Changelog": (
            "https://framagit.org/mieg/prgconfig/blob/master/CHANGES.md"
        ),
        "Issue Tracker": "https://framagit.org/mieg/prgconfig/issues",
    },
)
