"""Test if setup and special var in prgconfig are the same."""

import re

import prgconfig


def test_setup():
    """Test setup.py file."""
    values = {
        "name": prgconfig.__productname__,
        "version": prgconfig.__version__,
        "description": prgconfig.__description__,
        "author": prgconfig.__author__,
        "author_email": prgconfig.__author_email__,
        "url": prgconfig.__url__,
        "license": prgconfig.__license__,
    }
    with open("setup.py") as file:
        match = re.search(r"setup\((.*)\)", file.read(), re.S)
        setup = match.group(1)
    setup_lines = setup.split(",\n")
    for line in setup_lines:
        match = re.search(r"(\w+)=(.+)", line, re.S)
        if match:
            key, py_expr = match.group(1, 2)
            if key in values:
                # pylint: disable=eval-used
                assert values[key] == eval(py_expr)
