"""Test for prgconfig package"""

import os
import tempfile
from pathlib import Path

import pytest

import prgconfig as config


class TestPrgConfig:
    """Test suite for PrgConfig"""

    @staticmethod
    def test_xdg_constant():
        """Test XDG var"""
        os.environ["XDG_CONFIG_HOME"] = "/home/test/.config"
        os.environ["XDG_CONFIG_DIRS"] = "/etc/xdg1:/etc/xdg2:/etc/xdg3"
        conf = config.PrgConfig(prg_name="test")

        assert conf.xdg_config_home == Path("/home/test/.config")
        assert conf.xdg_config_dirs == [
            Path("/etc/xdg1"),
            Path("/etc/xdg2"),
            Path("/etc/xdg3"),
        ]

    @staticmethod
    def test_init():
        """Test init of a PrgConfig."""
        os.environ["XDG_CONFIG_HOME"] = "/home/test/.config"
        os.environ["XDG_CONFIG_DIRS"] = "/etc/xdg1:/etc/xdg2"

        prgname = "test-prg"
        conf = config.PrgConfig(prgname)
        assert conf.prg_name == prgname
        paths = []

        # Current directory
        paths.append(Path("{prg}.conf".format(prg=prgname)))
        paths.append(Path("{prg}.toml".format(prg=prgname)))
        paths.append(Path(".{prg}.conf".format(prg=prgname)))
        paths.append(Path(".{prg}.toml".format(prg=prgname)))

        # In xdg_config_home
        paths.append(Path("/home/test/.config/{prg}.conf".format(prg=prgname)))
        paths.append(Path("/home/test/.config/{prg}.toml".format(prg=prgname)))
        paths.append(
            Path("/home/test/.config/{prg}/config".format(prg=prgname))
        )
        paths.append(
            Path("/home/test/.config/{prg}/{prg}.conf".format(prg=prgname))
        )
        paths.append(
            Path("/home/test/.config/{prg}/{prg}.toml".format(prg=prgname))
        )

        # In home
        paths.append(Path().home() / ".{prg}.conf".format(prg=prgname))
        paths.append(Path().home() / ".{prg}.toml".format(prg=prgname))

        # In xdg_config_dirs
        paths.append(Path("/etc/xdg1/{prg}.conf".format(prg=prgname)))
        paths.append(Path("/etc/xdg1/{prg}.toml".format(prg=prgname)))
        paths.append(Path("/etc/xdg1/{prg}/config".format(prg=prgname)))
        paths.append(Path("/etc/xdg1/{prg}/{prg}.conf".format(prg=prgname)))
        paths.append(Path("/etc/xdg1/{prg}/{prg}.toml".format(prg=prgname)))
        paths.append(Path("/etc/xdg2/{prg}.conf".format(prg=prgname)))
        paths.append(Path("/etc/xdg2/{prg}.toml".format(prg=prgname)))
        paths.append(Path("/etc/xdg2/{prg}/config".format(prg=prgname)))
        paths.append(Path("/etc/xdg2/{prg}/{prg}.conf".format(prg=prgname)))
        paths.append(Path("/etc/xdg2/{prg}/{prg}.toml".format(prg=prgname)))

        # /etc
        paths.append(Path("/etc/{prg}.conf".format(prg=prgname)))
        paths.append(Path("/etc/{prg}.toml".format(prg=prgname)))
        paths.append(Path("/etc/{prg}/config".format(prg=prgname)))
        paths.append(Path("/etc/{prg}/{prg}.conf".format(prg=prgname)))
        paths.append(Path("/etc/{prg}/{prg}.toml".format(prg=prgname)))

        print(conf.config_file_path)
        for path in paths:
            assert path in conf.config_file_path
        assert paths == conf.config_file_path

    @staticmethod
    def test_load():
        """Test load function"""
        with tempfile.TemporaryDirectory("prgconfig-test") as tmpdir:
            # Create default config file
            default = Path(tmpdir) / "default"
            default.write_text(
                """
                default = "value"
                item = "val"
                foo = "bar"

                [section]
                    key1 = "val1"
                """
            )
            confone = Path(tmpdir) / "confone"
            confone.write_text(
                """
                item = "value"
                foo = 1
                """
            )
            conftwo = Path(tmpdir) / "conftwo"
            conftwo.write_text(
                """
                foo = 2
                """
            )
            conferror = Path(tmpdir) / "conferror"
            conferror.write_text(
                """
                foo = 2:3
                """
            )
            confwrong = Path(tmpdir) / "confwrong"
            config_file_path = [confone, confwrong, conftwo]
            conf = config.PrgConfig(
                prg_name="test",
                defaults_file=default,
                config_file_path=config_file_path,
            )
            conf.load()
            assert conf.config_sources == [confone, conftwo]
            assert conf.get("default") == "value"
            assert conf.get("item") == "value"
            assert conf.get("foo") == 1
            assert conf.get("section").get("key1") == "val1"
            # Test clear
            conf.clear()
            assert not conf
            assert not conf.config_sources
            # Test with merge set to false
            conf.merge = False
            conf.load()
            assert conf.config_sources == [confone]
            assert conf.get("default") == "value"
            assert conf.get("foo") == 1

    @staticmethod
    def test_load_with_errors():
        """Test that an error in config file correctly raise error"""
        with tempfile.TemporaryDirectory("prgconfig-test") as tmpdir:
            conferror = Path(tmpdir) / "conferror"
            conferror.write_text(
                """
                foo = 2:3
                """
            )
            conf = config.PrgConfig(
                prg_name="test", config_file_path=conferror
            )
            with pytest.raises(config.ParseError):
                conf.load()
            assert not conf.config_sources

    @staticmethod
    def test_defaults_with_errors():
        """Test a error in defaults file correctly raise error"""
        with tempfile.TemporaryDirectory("prgconfig-test") as tmpdir:
            conferror = Path(tmpdir) / "defaults"
            conferror.write_text(
                """
                foo = 2:3
                """
            )
            conf = config.PrgConfig(prg_name="test", defaults_file=conferror)
            with pytest.raises(config.ParseError):
                conf.load()
