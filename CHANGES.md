prgconfig CHANGES
=================


1.0.0-beta.1 (2020-01-05)
------------------------

* Rename project.
* Add test with other python version.


1.0.0-beta (2019-11-24)
-----------------------

* Fully tested.
* Use proper default file location for configuration file.
* Installation tested
* Need to be used by other applications before going to version 1.0.0.
